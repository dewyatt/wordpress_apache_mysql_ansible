# wordpress_apache_mysql_ansible

## Description

This deploys and configures a new Wordpress installation and a local MySQL server and also creates the database.

## Usage

1. Edit the file `settings.yaml` to set the MySQL username, password, and database name you would like to use.
2. `vagrant up`
3. Connect to the web interface and Wordpress should be up and running.

A log file `/root/checkip.log` will also be created and contain the HTML returned from checkip.dyndns.org. To run this again, you can run `vagrant provision` and a new entry will be appended.
